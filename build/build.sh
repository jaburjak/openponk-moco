#!/bin/bash

set -e

moco_dir="${CI_PROJECT_DIR}/moco"
release_type="${CI_COMMIT_BRANCH}"

if ! [ -d "$moco_dir" ]; then
	echo "$moco_dir is not a directory."
	exit 1
fi

find . -type f -name 'openponk-*-mac-ARM-*.zip' -print0 | while IFS= read -r -d $'\0' file; do
	mv "$file" "$(echo "$file" | sed 's/mac-ARM/mac_arm/')"
done

IFS=$'\n'
variants=($(find . -type f -name 'openponk-*.zip' | sed 's%./%%g' | sed 's/.zip//g'))
unset IFS

master_variant="linux"

for variant in "${variants[@]}"; do
	if (grep -q "$master_variant" <<< "$variant"); then
		echo "Building $variant..."
		directory="$(echo "$variant" | cut -d'-' -f1-4)"
		unzip -q "$variant.zip"
		cd "$directory/"
		./pharo/bin/pharo --headless image/openponk-class-editor.image eval --save "Metacello new baseline: 'Moco'; repository: 'tonel://$moco_dir/src'; load."
		./pharo/bin/pharo --headless image/openponk-class-editor.image eval --save "OPProjectSaveCommand compile: 'execute
	MocoOpenPonkSaveWorkaround applyTo: projectController.
	projectController saveProject' notifying: nil.
OPProjectSaveAsCommand compile: 'execute
	MocoOpenPonkSaveWorkaround applyTo: projectController.
	projectController saveProjectAs' notifying: nil.
OPUMLXMISpecsStorage class compile: 'primitiveTypeNamed: aPrimitiveName
	^ self primitivesMap at: aPrimitiveName ifAbsent: [ OPUMLPrimitiveType new name: aPrimitiveName; yourself ]' notifying: nil."
		cd image/
		rm -rf pharo-local/package-cache/
		rm -rf pharo-local/ombu-sessions/*
		rm -rf pharo-local/play-cache/*
		mv openponk-class-editor.image openponk-moco.image
		mv openponk-class-editor.changes openponk-moco.changes
		cd ../../
		master_variant="$variant"
	fi
done

for variant in "${variants[@]}"; do
	directory="$(echo "$variant" | cut -d'-' -f1-4)"

	if ! [ -d "$directory" ]; then
		echo "Extracting $variant.zip..."
		unzip -q "$variant.zip"
		if (grep -q 'mac_arm' <<< "$variant"); then
			mv openponk-class-editor-mac-ARM "$directory"
		fi
	fi

	cd "$directory/"

	echo "Packaging $variant..."

	if ! [ "$variant" = "$master_variant" ]; then
		rm -rf image/
		cp -a "../$(echo "$master_variant" | cut -d'-' -f1-4)/image" ./
	fi

	if (grep -q 'linux' <<< "$variant"); then
		mv openponk-class-editor openponk-moco
		sed -i 's/openponk-class-editor/openponk-moco/g' openponk-moco
		rm openponk-class-editor-pharo-ui
		rm README.txt
		cp ../README-linux.txt README.txt
	fi
	if (grep -q 'win' <<< "$variant"); then
		mv openponk-class-editor.bat openponk-moco.bat
		sed -i 's/openponk-class-editor/openponk-moco/g' openponk-moco.bat
		rm openponk-class-editor.ps1
		rm README.txt
		cp ../README-windows.txt README.txt
	fi
	if (grep -q 'mac' <<< "$variant"); then
		mv openponk-class-editor openponk-moco
		sed -i 's/openponk-class-editor/openponk-moco/g' openponk-moco
		sed -i 's/openponk-class-editor/openponk-moco/g' README.txt
	fi
	
	cd ../
done

for variant in "${variants[@]}"; do
	platform="$(echo "$variant" | cut -d'-' -f4)"
	directory="openponk-moco-$platform"
	archive="openponk-moco-${platform}-${release_type}.zip"
	echo "Compressing $archive..."
	mv "$(echo "$variant" | cut -d'-' -f1-4)" "$directory"
	zip -q -r "$archive" "$directory"
done
